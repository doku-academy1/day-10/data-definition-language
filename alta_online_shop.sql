-- create schema
create schema if not exists alta_online_shop;
Set search_path To alta_online_shop, public;

-- create table

create table users(
  id serial not null primary key,
  status smallint not null,
  tanggal_lahir date not null,
  gender char(1),
  created_at timestamp not null,
  updated_at timestamp not null
);

create table payment_methods(
  id serial not null primary key,
  name_payment varchar(255) not null,
  created_at timestamp not null,
  updated_at timestamp not null
);

create table transactions(
  id serial not null primary key,
  user_id int not null,
  payment_method_id int not null,
  status varchar(10) not null,
  total_qty int not null,
  total_price numeric(25,2) not null,
  created_at timestamp not null,
  updated_at timestamp not null
);

create table transaction_details(
	id serial not null primary key,
	transaction_id int not null,
	product_id int not null,
	status varchar(10),
	qty int not null,
	price numeric(25,2) not null,
	created_at timestamp not null,
	updated_at timestamp not null
);

create table products(
  id serial not null primary key,
  product_typeid int not null,
  operator_id int not null,
  product_desc_id int not null,
  code varchar(50) not null,
  name_product varchar(100) not null,
  status smallint not null,
  created_at date not null,
  updated_at date not null
);

create table product_descriptions(
  id serial not null primary key,
  description text,
  created_at timestamp not null,
  updated_at timestamp not null
);

create table product_types(
  id serial not null primary key,
  name_product_type varchar(255) not null,
  created_at timestamp not null,
  updated_at timestamp not null
);

create table operators(
  id serial not null primary key,
  operator_name varchar(255),
  created_at timestamp not null,
  updated_at timestamp not null
);

-- create relation foreign key

alter table transactions
add constraint fk_user_id
foreign key (user_id)
references users(id)
on delete cascade
on update cascade
;

alter table transactions
add constraint fk_payment_method_id
foreign key (payment_method_id)
references payment_methods(id)
;

alter table transaction_details
add constraint fk_product_id
foreign key (product_id)
references products(id)
;

alter table transaction_details
add constraint fk_transaction_id
foreign key (transaction_id)
references transactions(id)
;

alter table products
add constraint fk_product_type_id
foreign key (product_typeid)
references product_types(id) 
;

alter table products
add constraint fk_operator_id
foreign key (operator_id)
references operators(id)
;

alter table products
add constraint fk_product_desc_id
foreign key (product_desc_id)
references product_descriptions(id)
;

-- create kurir table

create table kurir(
    id serial not null primary key,
    kurir_name varchar(50) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);


-- add ongkos_dasar column

alter table kurir
add ongkos_dasar numeric(10,2);


-- rename kurir table to shipping

alter table kurir
rename to shipping;


-- delete shipping table
drop table shipping;


-- 1-to-1: payment method description

create table payment_method_descriptions(
    payment_method_id int not null primary key,
    payment_desc text not null,
    additional_charge numeric(8,2) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

alter table payment_method_descriptions
add constraint fk_payment_method_desc
foreign key (payment_method_id)
references payment_methods(id);


-- 1-to-many: user with alamat

create table alamat(
    id serial not null primary key,
    user_id int not null,
    address_detail text not null,
    postal_code varchar(10) not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

alter table alamat
add constraint fk_user_id_alamat
FOREIGN KEY (user_id)
REFERENCES users(id);


-- many-to-many: user with payment method to user_payment_method_detail

CREATE TABLE user_payment_method_detail(
    id serial not null primary key,
    user_id int not null,
    payment_method_id int not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

alter table user_payment_method_detail
add constraint fk_user_id_payment_method_detail
foreign key (user_id)
references users(id);

alter table user_payment_method_detail
add constraint fk_payment_method_id
foreign key (payment_method_id)
references payment_methods(id);